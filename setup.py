#!/bin/env python3
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'MarkupSafe',
    'onetimepass',
    'oauthlib',
    'requests-oauthlib',
    'passlib',
    'psycopg2',
    'python3-memcached',
    'pyramid',
    'pyramid_beaker',
    'pyramid_mailer',
    'pyramid_mako',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'pyramid_webassets',
    'pytz',
    'qrcode',
    'pillow',
    'shortuuid',
    'slowlog',
    'SQLAlchemy',
    'transaction',
    'waitress',
    'WebTest',
    'WTForms',
    'WTForms-Alchemy',
    'WTForms-Components',
    #'pyramid_wtforms',
    'zope.sqlalchemy',
    'autobahn',
    'beaker',
    'wsaccel',
    'aiohttp',
    'aiopg',
    'aiomcache            == 0.1',   # we are monkey patching this like mad
    ]

setup(name='davezdominoes',
      version='0.0.1',
      description='Davez Dominoes Game',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Other Audience",
        "License :: OSI Approved :: GNU General Public License v3 or later "\
                                   "(GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Games/Entertainment :: Board Games",
        ],
      platforms=['Linux'],
      author='David Moore',
      author_email='dave@dominoes.software.net.nz',
      url='http://dominoes.software.net.nz',
      license='GPL',
      keywords='web dominoes game',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite="tests",
      tests_require=["flexmock", "selenium"],
      install_requires=requires,
      entry_points="""
        [paste.app_factory]
          main = {gamecoordinator}:main

        [console_scripts]
          dominoes_gameserver = {gameserver}.app:main
          dominoes_init_db    = {gamecoordinator}.scripts.initializedb:main
          dominoes_post_mail  = {gamecoordinator}.scripts.postmail:main
          dominoes_webassets  = {gamecoordinator}.scripts.buildassets:main

      """.format(gamecoordinator = "davezdominoes.gamecoordinator",
                 gameserver      = "davezdominoes.gameserver")
      )
